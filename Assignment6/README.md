# Assignment 6 - Your First C Program

## By C2C Tylar Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Debugging](#debugging)
3. [Testing methodology or results](#testing-methodology-or-results)
4. [Answers to Lab Questions](#answers-to-lab-questions)
5. [Observations and Conclusions](#observations-and-conclusions)
6. [Documentation](#documentation)
 
### Objectives or Purpose 
Write a C program that does the following:

Creates three unsigned chars (val1, val2, val3). Initializes them to 0x40, 0x35, and 0x42.
Creates three unsigned chars (result1, result2, result3). Initializes them to 0.
Checks each number against a threshold value of 0x38.

If val1 is greater than the threshold value, stores the 10th Fibonacci number in result1 by using a for loop.
If val2 is greater than the threshold value, stores 0xAF to result2.
If val3 is greater than the threshold value, subtracts 0x10 from val2 and stores the result into result3.


Additional Requirements:

The threshold value must be a properly defined constant (refer to the lesson notes if you are confused by this requirement).
Comment your code. In particular, provide a good file header (example provided in lesson notes).

### Debugging
Originally my for loop was doing more than I thought it should. I realized my exit condition was wrong. When I figured that out the program worked.

### Testing methodology or results
##### Image 1: Values of Variables After Initializing Them
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/c16db9a39611fc94c1afd4d9c9c93d056d8b8156/Assignment6/images/initial_values.JPG?token=21f45608067f224bb133cac1925e94c4f83b9e10)

##### Image 2: Values of Variables at End of Program
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/c16db9a39611fc94c1afd4d9c9c93d056d8b8156/Assignment6/images/final_values.JPG?token=8d52dd3e02e3fb7aabf9370e9b47e35df0504124)

### Answers to Lab Questions
The address in memory where variables are stored can be seen in the location column of Image 1 and Image 2. To further reassure myself that those are the right locations I looked at the memory browser. Image 3 confirms the locations given in Image 1 and Image 2. Note that 34 decimal is 0x22.

##### Image 3: Memory Browser
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/60bb657fff09193a228b054279142b355b257552/Assignment6/images/memBrowser.JPG?token=302b7c677acc57d3d753e3ad14a7ed13eb227d44)

### Observations and Conclusions
The objective was to write a program in C that accomplished the description at the beginning of this README. This was completed. This was a good refresher on programming in C. I noticed that there are differences in some of the code that this compiler will accept compared to the compiler I used last time I programmed in C. 

### Documentation
None.