/*--------------------------------------------------------------------
Name: Tylar Hanson
Date: 5 Oct 16
Course: ECE 382 - Embedded Systems
File: main.c
HW: Assignment 6

Purp: First C Program, Uses if statements and a for loop.

Doc:   None

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#include <msp430.h> 
#define TV 0x38

int fibonacci(int n){
	// Returns the nth Fibonacci number
	int result = 0;
	int a = 0;
	int b = 1;
	if(n==2){ return b; }
	for(n; n>2; n--){
		result = a + b;
		a = b;
		b = result;
	}
	return result;
}

void main(void) {
    WDTCTL = WDTPW | WDTHOLD;						// Stop watchdog timer
    unsigned char val1 = 0x40;						// Initialize all the values
    unsigned char val2 = 0x35;
    unsigned char val3 = 0x42;
    unsigned char result1 = 0;
    unsigned char result2 = 0;
    unsigned char result3 = 0;

    if(val1>TV){									// Compare all the values to TV
    	result1 = fibonacci(10);					// Call the fibonacci() function
    }
	if(val2>TV){
		result2 = 0xAF;
	}
	if(val3>TV){
		result3 = val2 - 0x10;
	}
	while(1) {}										// CPU Trap
}

