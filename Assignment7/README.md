# Assignment 7 - Pong

## By C2C Tylar Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Debugging](#debugging)
3. [Testing methodology or results](#testing-methodology-or-results)
4. [Answers to Lab Questions](#answers-to-lab-questions)
5. [Observations and Conclusions](#observations-and-conclusions)
6. [Documentation](#documentation)
 
### Objectives or Purpose 

Write a C program that implements a subset of the functionality of the video "pong" game.

- Define the height/width of the screen as constants.
- Create a structure that contains the ball's parameters (x/y position, x/y velocity, radius).
- Make a function that creates a ball based on parameters passed into it.
- Make another function that updates the position of the ball (input is a ball structure, output is the updated ball structure).
    - This function must handle the "bouncing" of the ball when it hits the edges of the screen.
    - When it hits an edge, flip the sign on the x or y velocity to make the ball move a different direction.
    - This function should call other "collision detection" functions.
    - The "collision detection" functions return a `char` (1 for true, 0 for false - `#define` these values) to indicate whether or not there is a collision.
- Create three separate files: header, implementation, and `main.c`.

### Debugging
My xCollision() function should detect when the ball hits either the left or right wall. When simulating the ball wouldn't change direction after hitting the left or right wall. I realized the function was fine and the if else statements in moveBall() needed to be fixed. I changed it to look for hitting two edges at once, like in a corner, or any edge by itself.

### Testing methodology or results
For testing I set the ball's velocity to (10,10) and placed it in the center of the screen. My screen size for testing is 120 x 160. The ball is expected to change directions when it is within it's radius from any wall, so at x = 10 or 110, and y = 10 and 150. If it hits a left or right wall, its x velocity should be negative of what it was before. The same goes for the top and bottom boundaries and the y velocity.

##### Test 1: Left Boundary
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/919157f1497ef464d974f1809126ed409e26c689/Assignment7/images/leftEdgeCase0.JPG?token=b538891d8d48f11f09fd7b31113cbf6b23515591)
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/919157f1497ef464d974f1809126ed409e26c689/Assignment7/images/leftEdgeCase1.JPG?token=0920c47b4a1d9182cf455ccfc57e6e735ede2aaa)

The left image shows the ball heading towards the left wall, the right image shows the ball colliding with the wall and the x velocity changing.

##### Test 2: Bottom Boundary
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/919157f1497ef464d974f1809126ed409e26c689/Assignment7/images/bottomEdgeCase0.JPG?token=c8dd8c30ad07749d5b338250de93d23fbae083ee)
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/919157f1497ef464d974f1809126ed409e26c689/Assignment7/images/bottomEdgeCase1.JPG?token=607c7c563b60bf5eed0d53d8bf7dc64e8c8b4c86)

The left image shows the ball heading towards the bottom wall, the right image shows the ball colliding with the wall and the y velocity changing.

##### Test 3: Right Boundary
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/919157f1497ef464d974f1809126ed409e26c689/Assignment7/images/rightEdgeCase0.JPG?token=6cf37bb3bc1c921cd7774d2817fe33aea9144c87)
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/919157f1497ef464d974f1809126ed409e26c689/Assignment7/images/rightEdgeCase1.JPG?token=3e8ac78e08fc7022fd1ade8e20f75863840e7eaf)

The left image shows the ball heading towards the right wall, the right image shows the ball colliding with the wall and the x velocity changing.

##### Test 4: Top Boundary
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/919157f1497ef464d974f1809126ed409e26c689/Assignment7/images/topEdgeCase0.JPG?token=e47b2c8ad27d6f32cbf4f3e2454615e3776b8fe9)
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/919157f1497ef464d974f1809126ed409e26c689/Assignment7/images/topEdgeCase1.JPG?token=398458b3100414be271dda86ecec975584e97431)

The left image shows the ball heading towards the top wall, the right image shows the ball colliding with the wall and the y velocity changing.

### Answers to Lab Questions
I verified my code functions correctly by, in main.c, creating a ball, moving it continually, and by observing changes in position and velocity values in the variable window. These observations can be seen in the Testing Methodology section. In order to make helper functions not callable from `main.c` you would declare that function as static in the `pong.h` file. 

### Observations and Conclusions
The objective of this program was to implement the basic functionality of pong. I wrote functions that create a ball, move the ball, and detect collisions with any wall. The four test cases above show that I was able to make the ball, move it around, and successfully bounce it of the walls. I learned how to use nested structures, that's not something I've done before. While writing this I observed that a leftCollision() and rightCollision() function would have the same affect on the balls velocity, so I was able to combine them into one xCollision() function. The same goes for the top and bottom collision detection functions.

### Documentation
None. 