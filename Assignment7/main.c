/*--------------------------------------------------------------------
Name: Tylar Hanson
Date: 5 Oct 16
Course: ECE 382
File: main.c
Event: Assignment 7

Purp: Pong. Creates a ball with a position vector, a velocity vector,
and a radius. The function main() creates a ball and continually moves
it around.

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#include <msp430.h>
#include <stdlib.h>
#include "pong.h"

void main(void) {
	// Create a ball and continually move it around
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
	
    Ball myBall = createBall( SCREEN_WIDTH/2, SCREEN_HEIGHT/2, 10, 10, RADIUS);
    while( TRUE ){
    	myBall = moveBall( myBall );
    }
}
