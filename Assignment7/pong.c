/*--------------------------------------------------------------------
Name: Tylar Hanson
Date: 5 Oct 16
Course: ECE 382
File: pong.c
Event: Assignment 7

Purp: Includes functions to create a ball, move the ball, and detect
collisions with the screen edges.

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#include "pong.h"

Ball createBall( int xPos, int yPos, int xVel, int yVel, int radius ){
	volatile Ball result;
	result.pos.x = xPos;
	result.pos.y = yPos;
	result.vel.x = xVel;
	result.vel.y = yVel;
	result.radius = radius;
	return result;
}

Ball moveBall( Ball ballToMove ){
	volatile Ball result;
	result.pos.x = ballToMove.pos.x + ballToMove.vel.x;
	result.pos.y = ballToMove.pos.y + ballToMove.vel.y;

	if( xCollision(result) && yCollision(result) ){
		result.vel.x = ballToMove.vel.x * -1;
		result.vel.y = ballToMove.vel.y * -1;
	}
	else if( yCollision(result) ){
		result.vel.y = ballToMove.vel.y * -1;
		result.vel.x = ballToMove.vel.x;
		}
	else if( xCollision(result) ){
		result.vel.x = ballToMove.vel.x *-1;
		result.vel.y = ballToMove.vel.y;
	}
	else {
		result.vel.x = ballToMove.vel.x;
		result.vel.y = ballToMove.vel.y;
	}

	result.radius = ballToMove.radius;
	return result;
}

char xCollision( Ball checkMe ){
	char result = FALSE;
	if( (checkMe.pos.x >= SCREEN_WIDTH - checkMe.radius) | (checkMe.pos.x <= checkMe.radius) ){ result = TRUE; };
	return result;
}

char yCollision( Ball checkMe ){
	char result = FALSE;
	if( (checkMe.pos.y >= SCREEN_HEIGHT - checkMe.radius) | (checkMe.pos.y <= checkMe.radius) ){ result = TRUE; };
	return result;
}
