/*--------------------------------------------------------------------
Name: Tylar Hanson
Date: 5 Oct 16
Course: ECE 382
File: pong.h
Event: Assignment 7

Purp: Header file for pong program.

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#ifndef PONG_H_
#define PONG_H_

#define SCREEN_HEIGHT 320/2
#define	SCREEN_WIDTH 240/2
#define	TRUE 1
#define FALSE 0
#define RADIUS 10

typedef struct {
	int x;
	int y;
}Vector2d;

typedef struct {
	Vector2d pos;
	Vector2d vel;
	int radius;
}Ball;

// Create a ball at (xPos, yPos) with a velocity of (xVel, yVel) and a radius.
Ball createBall( int xPos, int yPos, int xVel, int yVel, int radius );

// Update a ball's position and check for collisions with any wall.
Ball moveBall( Ball ballToMove );

// Check for collision with left or right wall; returns TRUE on collision, FALSE if otherwise.
static char xCollision( Ball checkMe );

// Check for collision with top or bottom wall; returns TRUE on collision, FALSE if otherwise.
static char yCollision( Ball checkMe );

#endif
