# Assignment 8 - Moving Average

## By C2C Tylar Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Testing methodology or results](#testing-methodology-or-results)
3. [Answers to Lab Questions](#answers-to-lab-questions)
4. [Documentation](#documentation)
 
### Objectives or Purpose 
- Write a function that computes the "moving average" for a stream of data points it is receiving.
    - A moving average computes the average of the most recent `N_AVG_SAMPLES` points.
    - Initialize values in the samples array to be 0.
    - For example: (`N_AVG_SAMPLES` = 2), sample stream = (2, 4, 6, 8).
        - Starting point: average [0, 0]
            - Average is 0
        - First sample: average [0, 2]
            - Average is 1
        - Second sample: average [2, 4]
            - Average is 3
        - Third sample: average [4, 6]
            - Average is 5
        - Fourth sample: average [6, 8]
            - Average is 7
    - Use the following data streams to test your moving average function:
        - Test these streams with (`N_AVG_SAMPLES` = 2) and (`N_AVG_SAMPLES` = 4).
        - (45, 42, 41, 40, 43, 45, 46, 47, 49, 45)
        - (174, 162, 149, 85, 130, 149, 153, 164, 169, 173)
- Write three other functions that compute the maximum, minimum, and range of a given array.

### Testing methodology or results
For each case below the data stream can be seen in testArray[], and the number of points to average can be seen in the type of samplesArray, int[x]. X is the number of sample point to average. Each updated moving average is kept in the movingAverages[] array. For example in the first test vector the very first sample is [0,0] so the average is 0. When the first value of 45 is moved into the sample the average becomes floor(0+45)/2, which is 22. The answer is truncated because I'm using an integer data type. The max, min, and range is also calculated for the movingAverages[] array at the end of the program for each test vector.
##### Figure 1: First Test Vector
This is the result for a data stream of (45, 42, 41, 40, 43, 45, 46, 47, 49, 45) with `N_AVG_SAMPLES` = 2
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/5fcb1a33473e5555773053ea19fe584b0ff6004e/Assignment8/images/TestCase1.JPG?token=9ac173b62936f56d6f08a1ad43efce8f5738455e)
##### Figure 2: Second Test Vector
This is the result for a data stream of (45, 42, 41, 40, 43, 45, 46, 47, 49, 45) with `N_AVG_SAMPLES` = 4
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/8b4a45bff473283c665b3471ac602f16fb1ae7e2/Assignment8/images/TestCase1_2.JPG?token=878dad57c3831f8dffad019d1a6e34f0e491c3e4)
##### Figure 3: Third Test Vector
This is the result for a data stream of (174, 162, 149, 85, 130, 149, 153, 164, 169, 173) with `N_AVG_SAMPLES` = 4
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/5fcb1a33473e5555773053ea19fe584b0ff6004e/Assignment8/images/TestCase2.JPG?token=c3aa3dd9b2fe523ced7aa60ca2ceedbf12f1f00f)
##### Figure 4: Fourth Test Vector
This is the result for a data stream of (174, 162, 149, 85, 130, 149, 153, 164, 169, 173) with `N_AVG_SAMPLES` = 2
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/8b4a45bff473283c665b3471ac602f16fb1ae7e2/Assignment8/images/TestCase2_2.JPG?token=8518753d95df8c22b0436a1b2f414e51bc6c73a1)

### Answers to Lab Questions
- I used integers for the data type of my arrays because the data can be positive or negative numbers.
- The min, max, and range functions were tested by passing the movingAverages array to each function and making sure they returned what I expected them to.
- 
### Documentation
None. 