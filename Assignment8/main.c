/*--------------------------------------------------------------------
Name: Tylar Hanson
Date: 7 Oct 16
Course: ECE 382
File: main.c
Event: Assignment 8

Purp: Functions to update and monitor a moving average

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#include <msp430.h> 
#include "moving_avg.h"

void main(void) {
	WDTCTL = WDTPW | WDTHOLD;
	volatile int testArray[numTestValues] = {45, 42, 41, 40, 43, 45, 46, 47, 49, 45};

	// Initialize moving averages array to zero
	int movingAverages[numTestValues+1] = {0};
	unsigned int movAvgArrayLength = 0;

	// Initialize values in a samples array to be zero
	int samplesArray[N_AVG_SAMPLES] ={0};

	// Take in data points, update moving average
	unsigned int i = 0;
	while(i < numTestValues){
		movAvgArrayLength++;
		addValToEnd( movingAverages, movAvgArrayLength, findAvg( samplesArray, N_AVG_SAMPLES ) );
		insertItem( samplesArray, N_AVG_SAMPLES, testArray[i]);
		i++;
	}

	// Test min, max, and range functions
	volatile int min, max, range;
	min = findMinVal(movingAverages, movAvgArrayLength);
	max = findMaxVal(movingAverages, movAvgArrayLength);
	range = findRange(movingAverages, movAvgArrayLength);
	while(1);
}
