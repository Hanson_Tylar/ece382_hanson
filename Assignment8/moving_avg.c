/*--------------------------------------------------------------------
Name: Tylar Hanson
Date: 7 Oct 16
Course: ECE 382
File: moving_avg.c
Event: Assignment 8

Purp: Functions to update and monitor a moving average.

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#include "moving_avg.h"

void insertItem ( int array[], unsigned int arrayLength, int value ){
	unsigned int i = 0;
	while(i < arrayLength-1){
		array[i] = array[i + 1];
		i++;
	}
	array[i] = value;
	return;
}
void addValToEnd( int array[], unsigned int arrayLength, int value ){
	array[arrayLength-1] = value;
	return;
}
int findMaxVal( int array[], unsigned int arrayLength ){
	int max = array[0];
	unsigned int i;
	for(i=0; i < arrayLength; i++){
		if(array[i] > max){
			max = array[i];
		}
	}
	return max;
}

int findMinVal( int array[], unsigned int arrayLength ){
	int min = array[0];
	unsigned int i;
	for(i=0; i < arrayLength; i++){
		if(array[i] < min){
			min = array[i];
		}
	}
	return min;
}

unsigned int findRange( int array[], unsigned int arrayLength ){
	return findMaxVal(array, arrayLength) - findMinVal(array, arrayLength);
}

int findAvg( int array[], unsigned int nTerms ){
	int sum = 0;
	unsigned int i = 0;
	while(i < nTerms){
		sum += array[i];
		i++;
	}
	return sum/nTerms;
}

