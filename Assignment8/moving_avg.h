/*--------------------------------------------------------------------
Name: Tylar Hanson
Date: 7 Oct 16
Course: ECE 382
File: moving_avg.h
Event: Assignment 8

Purp: Functions to update and monitor a moving average

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#ifndef MOVING_AVG_H_
#define MOVING_AVG_H_
#include <stdlib.h>
#include <stdio.h>
#define N_AVG_SAMPLES 4
#define numTestValues 10

// Insert an item into a given list, shifts in a value on the right
void insertItem( int array[], unsigned int arrayLength, int value );

// Add a value to the end of a given list
void addValToEnd( int array[], unsigned int arrayLength, int value );

// Find maximum value in given list
int findMaxVal( int array[] , unsigned int arrayLength);

// Find minimum value in given list
int findMinVal( int array[], unsigned int arrayLength );

// Find the range of given list
unsigned int findRange( int array[], unsigned int arrayLength );

//Find the average of a given list
int findAvg( int array[], unsigned int arrayLength );

#endif
