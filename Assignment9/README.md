# Assignment 9 - Timers And Interrupts

## By C2C Tylar Hanson

## Table of Contents
1. [Answers to Lab Questions](#answers-to-lab-questions)
2. [Documentation](#documentation)

### Answers to Lab Questions
- I chose to do interrupts for the buttons instead of the delay.
- To verify the interrupts for the push buttons on the pong game worked I loaded the program onto the board. The ball still moves as it did before and when the left or right button is pushed the paddle moves. The paddle doesn't move continuously as the button is held down though. This is because the interrupt triggers on the falling edge of the pin. To change this I would have to make the interrupt state sensitive instead of edge sensitive,  but I couldn't find how to do this anywhere.

### Documentation
None.