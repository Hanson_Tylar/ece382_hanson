# Lab #1 - Assembly Language - "A Simple Calculator"

## By C2C Tylar Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
4. [Debugging](#debugging)
5. [Testing methodology or results](#testing-methodology-or-results)
6. [Observations and Conclusions](#observations-and-conclusions)
7. [Documentation](#documentation)
 
### Objectives or Purpose 
The purpose of this lab is to write a program that interprets a series of operands and operations and store the results. the program reads a set of instructions from ROM and stores all the results in RAM. This assembly calculator adds, subtracts, and multiplies values up to one byte in size. Values are not to exceed 255 or be less than 0.

### Preliminary design
The design begins by creating a flowchart of pseudo code in order to get a good idea of how the program should be out together. 
	
### Software flow chart or algorithms

##### Figure 1: Prelab Flowchart
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/eff3c0a625cf261083e69c4f86ee4a0073938b39/Lab1/Lab1_FlowChart.png?token=4b1c34060a8ac4f9839ccac055f1c4f289c4ba0b)

### Debugging
I ran into a problem with my clear operation. I realized I was not using the second operand as the first operand in the next instruction. 
When trying to complete the B functionality my program was not doing what I expected it to do with numbers greater than 255 and less than 0. I realized that when my addition and subtract methods did math the biggest result they could store is 0xFF before being overflowed. To fix this I changed add.b and sub.b to add.w and sub.w to give me more space to work with. 

### Testing methodology or results
Three test vectors were given to test the three levels of functionality required in this program. These test vectors and the results in memory are shown below.

##### Figure 2 Test Vector 1
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/6b8f92f80f6e3abd647fdb591622485d4aa494d1/Lab1/TestVector1.png?token=488699857c85f25deb5a513d1e547bf23f030a74)
##### Figure 3 Test Vector 2
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/6b8f92f80f6e3abd647fdb591622485d4aa494d1/Lab1/TestVector2.png?token=f08673548422950767e453577f1a90c42a675da4)
##### Figure 4 Test Vector 3
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/6b8f92f80f6e3abd647fdb591622485d4aa494d1/Lab1/TestVector3.png?token=64783c9e873079fee205a40f7a61a2ce911747b2)

I added two of my own test cases to further test my program. My first test case tested the store results function for edge cases like results equaling 0, 255, -1, and 256. My second test case tested the multiply function. I tested operations like a value times one, and a value times zero. The results in memory matched what I expect them to be so I know this program is correct.

##### Figure 5 My Test Vector 1
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/6b8f92f80f6e3abd647fdb591622485d4aa494d1/Lab1/MyTestVector1.png?token=53b1a0d074a8c923cbfb582b09f1f4ef028aef71)
##### Figure 6 My Test Vector 2
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/6b8f92f80f6e3abd647fdb591622485d4aa494d1/Lab1/MyTestVector2.png?token=bffbe2134c7b7f3faf22f9b2f3714e25460cfb81)

### Observations and Conclusions
The purpose of this lab was to create a simple assembly calculator. I was able to successfully create add, subtract, multiply, and clear functions. I learned how to use pointers in assembly, use if else statements, and how to use loops. 

### Documentation
None.