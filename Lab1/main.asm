;-------------------------------------------------------------
; Lab1 - A Simple Calculator
; C2C Tylar Hanson, USAF / 06 Sept 2016 / 11 Sept 2016
;
; A program that interprets a series of operands and operations
; and stores the results.
;-------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.


;myProgram:	.byte	0x11, 0x11, 0x11, 0x11, 0x11, 0x44, 0x22, 0x22, 0x22, 0x11, 0xCC, 0x55 ;Required Functionality Test
;myProgram:	.byte	0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0xDD, 0x44, 0x08, 0x22, 0x09, 0x44, 0xFF, 0x22, 0xFD, 0x55 ;B Functionality
;myProgram:	.byte	0x22, 0x11, 0x22, 0x22, 0x33, 0x33, 0x08, 0x44, 0x08, 0x22, 0x09, 0x44, 0xff, 0x11, 0xff, 0x44, 0xcc, 0x33, 0x02, 0x33, 0x00, 0x44, 0x33, 0x33, 0x08, 0x55
;myProgram:	.byte	0x00, 0x11, 0xFF, 0x44, 0x01, 0x11, 0xFF, 0x44, 0x10, 0x22, 0x10, 0x44, 0x10, 0x22, 0x11, 0x55 ; MyTestCase 1
myProgram:	.byte	0xFF, 0x33, 0x00, 0x44, 0xFF, 0x33, 0x01, 0x44, 0x01, 0x33, 0x01, 0x44, 0x00, 0x33, 0x00, 0x55 ; MyTestCase 2

ADD_OP:		.equ	0x11
SUB_OP:		.equ	0x22
CLR_OP:		.equ	0x44
END_OP:		.equ	0x55
MUL_OP:		.equ	0x33
MAX_NUM:	.equ	255
MIN_NUM:	.equ	0

			.data
myResults:	.space	20

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------

main:
			mov.w		#myProgram, r4	;Initialize ROM pointer
			mov.w		#myResults, r5	;Initialize RAM pointer
			mov.b		@r4+, r6		;Store first operand

keep_going:
			mov.b		@r4+, r7		;Store operation
			cmp			#END_OP, r7		;Is operation END?
			jz			forever			;If so, end program
			mov.b		@r4+, r8		;Store the second operand
			cmp			#CLR_OP, r7		;Else, Is operation clear?
			jz			Clear_Op		;If so, jump to Clear_Op
			cmp			#ADD_OP, r7		;Else, is operation addition?
			jz			Addition_Op		;If so, jump to addition_op
			cmp			#SUB_OP, r7		;Else, is operation subtraction?
			jz			Subtraction_Op	;If so, jump to subtraction_op
			cmp			#MUL_OP, r7		;Else, is operation multiplication?
			jz			Multiply_Op		;If so, jump to multiply_op
			jmp			Clear_Op		;Else, unknown operation, go to clear_op

Clear_Op:
			mov.b 		r8, r6			;Second operand becomes first operand for next instruction
			mov.b 		#0, 0(r5)		;Store zero in RAM
			inc 		r5				;Increment RAM pointer
			jmp			keep_going

Addition_Op:
			add.w		r8, r6			;Add the two operands, store result as first operand for next instruction
			jmp			Store_Result

Subtraction_Op:
			sub.w		r8, r6			;Subtract the two operands, store result as first operand for next instruction
			jmp			Store_Result

Multiply_Op:
			cmp			#0, r6			;If either operand zero jump to mul by zero
			jz			Mul_By_Zero
			cmp			#0, r8
			jz			Mul_By_Zero
			mov.b		r6, r9			;Start a loop counter
			clr			r6				;Clear r6 to keep intermediate results in it
			add.w		r8, r6

Add_Loop:
			dec			r9
			jz			Store_Result	;when counter reaches zero stop adding
			add.w		r8, r6
			jmp			Add_Loop

Mul_By_Zero:
			mov.b		#0, r6
			jmp			Store_Result

Store_Result:
			cmp			#MAX_NUM+1, r6	;Is result greater than MAX_NUM
			jge			Greater_Than_Max
			cmp			#MIN_NUM, r6	;Is result less than MIN_NUM
			jl			Less_Than_Min

Continue_Store:
			mov.b		r6, 0(r5)		;Store result in RAM
			inc			r5				;Increment RAM pointer
			jmp			keep_going

Greater_Than_Max:
			mov.b		#MAX_NUM, r6	;Set result to MAX_NUM
			jmp			Continue_Store

Less_Than_Min:
			mov.b		#MIN_NUM, r6	;Set result to MIN_NUM
			jmp			Continue_Store

forever: 	jmp forever

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
