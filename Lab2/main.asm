;-------------------------------------------------------------
; Lab2 - Subroutines "Cryptography"
; C2C Tylar Hanson, USAF / 12 Sept 2016 / 12 Sept 2016
;
; A program that decrypts an encrypted message using a
; simple encryption technique
;-------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file

;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section
            .retainrefs                     ; Additionally retain any sections
                                            ; that have references to current
                                            ; section
;Rquired Functionality 3ength 94   kay 0xac
;cipher_text:	.byte	0xef,0xc3,0xc2,0xcb,0xde,0xcd,0xd8,0xd9,0xc0,0xcd,0xd8,0xc5,0xc3,0xc2,0xdf,0x8d,0x8c,0x8c,0xf5,0xc3,0xd9,0x8c,0xc8,0xc9,0xcf,0xde,0xd5,0xdc,0xd8,0xc9,0xc8,0x8c,0xd8,0xc4,0xc9,0x8c,0xe9,0xef,0xe9,0x9f,0x94,0x9e,0x8c,0xc4,0xc5,0xc8,0xc8,0xc9,0xc2,0x8c,0xc1,0xc9,0xdf,0xdf,0xcd,0xcb,0xc9,0x8c,0xcd,0xc2,0xc8,0x8c,0xcd,0xcf,0xc4,0xc5,0xc9,0xda,0xc9,0xc8,0x8c,0xde,0xc9,0xdd,0xd9,0xc5,0xde,0xc9,0xc8,0x8c,0xca,0xd9,0xc2,0xcf,0xd8,0xc5,0xc3,0xc2,0xcd,0xc0,0xc5,0xd8,0xd5,0x8f
;B Functionality : Length 81   key 0xac, 0xdf, 0x23
cipher_text:	.byte	0xf8,0xb7,0x46,0x8c,0xb2,0x46,0xdf,0xac,0x42,0xcb,0xba,0x03,0xc7,0xba,0x5a,0x8c,0xb3,0x46,0xc2,0xb8,0x57,0xc4,0xff,0x4a,0xdf,0xff,0x12,0x9a,0xff,0x41,0xc5,0xab,0x50,0x82,0xff,0x03,0xe5,0xab,0x03,0xc3,0xb1,0x4f,0xd5,0xff,0x40,0xc3,0xb1,0x57,0xcd,0xb6,0x4d,0xdf,0xff,0x4f,0xc9,0xab,0x57,0xc9,0xad,0x50,0x80,0xff,0x53,0xc9,0xad,0x4a,0xc3,0xbb,0x50,0x80,0xff,0x42,0xc2,0xbb,0x03,0xdf,0xaf,0x42,0xcf,0xba,0x50
;A Functionality  : length 42
;cipher_text:	.byte	0x35,0xdf,0x00,0xca,0x5d,0x9e,0x3d,0xdb,0x12,0xca,0x5d,0x9e,0x32,0xc8,0x16,0xcc,0x12,0xd9,0x16,0x90,0x53,0xf8,0x01,0xd7,0x16,0xd0,0x17,0xd2,0x0a,0x90,0x53,0xf9,0x1c,0xd1,0x17,0x90,0x53,0xf9,0x1c,0xd1,0x17,0x9e
msg_length:		.equ	81
key:			.byte	0xac, 0xdf, 0x23
key_length:		.equ	3

			.data
plain_text:		.space	100
;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer

;-------------------------------------------------------------------------------
                                            ; Main loop here
;-------------------------------------------------------------------------------
			clr		r10

main:
			mov		#cipher_text, r4
			mov		#msg_length, r5
			mov		#key, r6
			mov		#key_length, r7      ; load registers with necessary info for decryptMessage here
			mov		#plain_text, r8
			mov		#0x025B, r12		;Location in RAM where key guesses will be kept.
			cmp		#0, r7				;If key length is zero, I'm working with an unknown key
			jz		unknown_key

continue_main:
            call    #decryptMessage
            cmp		#1, r11				;r11 will be one if the decrypted byte isn't an ascii character I'm looking for
            jz		main				;If one, start over with a differnt key

unknown_key:
			mov		#1, r10				;Used later to know if im working with an unknown key
			inc		0(r12)				;Start guessing with keys at one
			mov		#2, r7				;unknown key is two bytes long
			jmp		continue_main

forever:    jmp     forever

begin_key:								;This section manages the key pointer
			mov		#key_length, r7
			mov		#key, r6
			mov		#0x025B, r12
			jmp		continue_decryptMessage

;-------------------------------------------------------------------------------
                                            ; Subroutines
;-------------------------------------------------------------------------------

;-------------------------------------------------------------------------------
;Subroutine Name: decryptMessage
;Author: Tylar Hanson
;Function: Decrypts a string of bytes and stores the result in memory.  Accepts
;           the address of the encrypted message, address of the key, and address
;           of the decrypted message (pass-by-reference).  Accepts the length of
;           the message by value.  Uses the decryptCharacter subroutine to decrypt
;           each byte of the message.  Stores the results to the decrypted message
;           location.
;Inputs: r4, r5, r6
;Outputs: Hex byte into memory
;Registers destroyed: None
;-------------------------------------------------------------------------------

decryptMessage:
			mov.b	@r4+, r9			;store ciphertext byte, increment ciphertext pointer
			call 	#decryptCharacter
			cmp		#0x20 , r9			;
			jl		char_not_ascii		;Check to see if plaintext byte is an ascii character
			cmp		#0x7B, r9			;
			jge		char_not_ascii		;
			mov.b	r9, 0(r8)			;store plaintext byte in RAM
			inc		r8					;increment plaintext pointer
			dec		r7					;decrement length of key counter
			jz		begin_key			;if zero, rotate back to beginning of the key
			jmp		continue_decryptMessage	;else continue

char_not_ascii:
			mov		#1, r11				;This will tell main that the current key is wrong and will start the decryption over
			ret

continue_decryptMessage:
			dec 	r5					;decrement bytes remaining counter
			jnz		decryptMessage		;Keep going if whole message isn't decrypted.
            ret

;-------------------------------------------------------------------------------
;Subroutine Name: decryptCharacter
;Author:
;Function: Decrypts a byte of data by XORing it with a key byte.  Returns the
;           decrypted byte in the same register the encrypted byte was passed in.
;           Expects both the encrypted data and key to be passed by value.
;Inputs:
;Outputs:
;Registers destroyed:
;-------------------------------------------------------------------------------

decryptCharacter:
			cmp		#1, r10		;r10 will be one if were working with an unknown key
			jz		decrypt_with_unk_key
			jmp		decrypt_with_known_key

decrypt_with_unk_key:
			xor.b	@r12+, r9	;r12 has the memory address for a key starting at zero
			ret
decrypt_with_known_key:
			xor.b	@r6+, r9	;r6 stores the memory address of a given key
            ret

;-------------------------------------------------------------------------------
;           Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect    .stack

;-------------------------------------------------------------------------------
;           Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
