# Lab 3 - SPI - "I/O"

## By C2C Tylar Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
3. [Flowchart](#flowchart)
4. [Delay Subroutine](#delay-subroutine)
5. [Logic Analyzer](#logic-analyzer)
6. [Debugging](#debugging)
7. [Testing methodology or results](#testing-methodology-or-results)
8. [Prelab](#prelab)
9. [Observations and Conclusions](#observations-and-conclusions)
10. [Documentation](#documentation)
 
### Objectives or Purpose 
The purpose of this lab is to learn more about how to configure the MSP430 to use peripheral interfaces and demonstrate what we have learned about USCI and SPI through the use of an MSP430 and an LCD Booster Pack. The required functionality of the lab is to draw a 10x10 pixel box on the screen. To get A functionality I have to move that box left, right, up, or down by ten pixels when buttons two through five are pressed on the LCD pack. Bonus functionality requires me to write my name on the LCD.

### Preliminary design
This lab started with a Mega Prelab. In the prelab I filled in charts and studied lines of code to better understand how the MSP430 works with the LCD. These charts included what pin certain signals are on, as well as their functions. I was given the WriteCommand, WriteData, and DrawPixel subroutines. I drew a timing diagram of the expected pin behavior for the first two subroutines and for the third I explained what the purpose of each subroutine called in DrawPixel was. Also, a software delay subroutine was made as part of the prelab. Accomplishing all these tasks helped me gain a better understanding of how the MSP430 communicated with an LCD screen. Also, as part of preliminary design I created a flowchart of how I think the program should flow.

### Flowchart
##### Pseudocode Flowchart
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/092fc0e9e59a573697505546f1eeda520aa175cb/Lab3/images/Lab3Flowchart.png?token=f7d6d0304e10d5061990b9a85d26862df321686b)
### Delay Subroutine
The first part of the prelab was to create a delay subroutine that is 160ms long. In order to do this I need to know the actual frequency of my clock. The waveform below shows the period for one cycle of the SMCLK on my MSP430.

##### SMCLK Waveform
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/6d6bb980c2bc14b520be310d4fd7755f6e02aa5f/Lab3/images/SMCLK.jpg?token=120d6e04ff92cebd34a856680455e4ddaf48442f) 

The period for one cycle of my clock is 98 ns, shown in top left of image. Frequency is 1/T, so my frequency is 10.2041 MHz. The comments in the subroutine below show the algebra used to calculate how many loops I would need to get a 160ms delay. Starting with 0xD49F, I used the logic analyzer to get closer to a 160ms delay.

    ;-------------------------------------------------------------------------------
	;	Name: Delay160ms
	;	Inputs: none
	;	Outputs: none
	;	Purpose: creates ~160ms delay
	;	Registers: r7, r8 preserved
	;-------------------------------------------------------------------------------
	
	Delay160ms:					;5 cycles for the call
		push	r7				;3 cycles
		push	r8				;3
		mov.w	#10, r8			;2
	reset:
		mov.w	#0xd783, r7		;2 cycles
	delay:
		dec		r7          	;1 cycle
		jnz     delay			;2 cycles
		dec		r8				;1
		jnz		reset			;2
		pop		r8				;2
		pop     r7				;2 cycles
		ret						;3 cycles
	
		;.160 seconds = ( 1 s / 10.2041e6 cycles ) * 1.633e6 cycles
		;(1.633e6 - 68)/30 = num_loops
		;num_loops = 54431
		;54431 = 0xD49F
		;5+3+3+2+10*5+0xD49F*3*10+2+3 = 1632998 cycles
		;1632638/10.2041e6 = 160.034 ms
	
	;-------------------------------------------------------------------------------

##### Length of Delay160ms subroutine
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/6d6bb980c2bc14b520be310d4fd7755f6e02aa5f/Lab3/images/Delay.jpg?token=2137ee7e3d794752ee175f174b4da75b41b92830)

By changing the number of loops to 0xD783 I was able to create a delay of 159.995 ms.
### Logic Analyzer
The following images are wave forms taken from a logic analyzer. Each image shows the eight bit packet of data being sent to the LCD. These packets are the eleven packets sent to the LCD in the setArea subroutine following a button press. To read the data being sent we need to know that the clock has a phase of one. This means that on the first edge of the clock the data is being captured and on the second edge of the clock the data is being changed. So for these wave forms the bit of data actually being read by the LCD can be seen on each rising edge of the clock.
##### Packet 1
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/6d6bb980c2bc14b520be310d4fd7755f6e02aa5f/Lab3/images/Line1.jpg?token=86a07f0ef18f45eb20ec570e79b238d71674d3dc)
##### Packet 2
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/6d6bb980c2bc14b520be310d4fd7755f6e02aa5f/Lab3/images/Line2.jpg?token=012ff9362c470af94cc9aeb86abc1a4fff75fb3a)
##### Packet 3
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/6d6bb980c2bc14b520be310d4fd7755f6e02aa5f/Lab3/images/Line3.jpg?token=87c2f14fe975706231738e6153891cd227027958)
##### Packet 4
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/6d6bb980c2bc14b520be310d4fd7755f6e02aa5f/Lab3/images/Line4.jpg?token=ac6911d4a4b66fa095f6985aae412b39e41841b2)
##### Packet 5
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/6d6bb980c2bc14b520be310d4fd7755f6e02aa5f/Lab3/images/Line5.jpg?token=cdb9849612bd3302a9ce62a8759c7162d6727f59)
##### Packet 6
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/6d6bb980c2bc14b520be310d4fd7755f6e02aa5f/Lab3/images/Line6.jpg?token=a7325f7334f032efa8e2afab97324e2bbf330633)
##### Packet 7
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/6d6bb980c2bc14b520be310d4fd7755f6e02aa5f/Lab3/images/Line7.jpg?token=fcb5bf5d0b0d4b469300248a660f3bc207355256)
##### Packet 8
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/6d6bb980c2bc14b520be310d4fd7755f6e02aa5f/Lab3/images/Line8.jpg?token=ee2f907919a580056796dd198e788ecedb46511f)
##### Packet 9
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/6d6bb980c2bc14b520be310d4fd7755f6e02aa5f/Lab3/images/Line9.jpg?token=f6273ad22089a4ef54bb33db65985b7ea1ea43cf)
##### Packet 10
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/6d6bb980c2bc14b520be310d4fd7755f6e02aa5f/Lab3/images/Line10.jpg?token=e8e288c8baef6fd4a82682eaddf8cea357dad0c6)
##### Packet 11
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/6d6bb980c2bc14b520be310d4fd7755f6e02aa5f/Lab3/images/Line11.jpg?token=27d7266d688ed2de71ce1eb5cdad2735c824d160)

##### Logic Analyzer Waveform Results Table
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/6d6bb980c2bc14b520be310d4fd7755f6e02aa5f/Lab3/images/LogicAnalyzer.JPG?token=ffa88239097021cbfe67cd0b4247f57c3e40693d)

Lines one, six, and eleven of the above table are command information being sent to the LCD. All the other lines are packets of data. These packets tell the LCD where the line will be drawn. The LCDs orientation is set up so that point (0,0) is at the top left of the screen and that positive x is to the right and positive y is down. Lines two, three, seven, and eight tell the LCD that the left end point of the line is at (0x25, 0x8D). Lines four, five, nine, and ten tell the LCD that the right endpoint of the line is at point (0x2A, 0x8D). So the line drawn is a horizontal and five pixels long. 

### Debugging
In my drawBox subroutine I was getting negative lengths for the sides of the box. I fixed this by swapping the coordinate values so the result would be positive. When writing the draw name method I forgot to use mov.b instead of mov when copying the (x,y) coordinates into registers from memory.

### Testing methodology or results
When testing my program I expect each of the five buttons to do a certain task. S1 calls for a box to be drawn on the screen. S2 moves the box left by ten pixels. S3 moves the box down by ten pixels. S4 moves the box up by ten pixels. S5 moves the box right by ten pixels. When the box reaches the edge of the LCD display and I attempt to push the box further into the edge I expect the box to appear on the other edge of the screen. For example, when the box is first drawn it appears at the very top left corner. If I press the up button I expect the box to appear on the bottom left edge of the screen.  

When running my program a box doesn't appear until I press S1, so I know my DrawBox subroutine is funtional. When pressing the directional buttons the box moves in that direction after a short delay. The box behaves as I expected, even on the edges of the screen, so I know this part of the program is correct.

##### Box created, moved right, down, left, and up
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/68345e4fe8248f7c8930cf735d69ab4199c88690/Lab3/images/MovingBox.jpg?token=27fa22dc84d41dbe007e22a595cbb95173e4ca01)

##### Bonus Functionality
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/68345e4fe8248f7c8930cf735d69ab4199c88690/Lab3/images/MyName.jpg?token=0b298122f408491c0f62113a4f77226ddf4e96aa)

### Prelab 
This table shows the hex values that need to be combined with each registers in order for the given signal to be properly configured. Originally I only listed the hex values for the first row. In this chart I add the hex values for the other two rows.
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/a409d89fc8541cf096e2dda1bf148325bd1e8e3a/Lab3/images/prelab_correction.JPG?token=13c43b7e6c33e388943daaf94663d6cc77f1241a)

##### Writing Modes
![](https://bytebucket.org/Hanson_Tylar/ece382_hanson/raw/3f5eefda6a658fe4f6eb099d2c20198d5adaca0c/Lab3/images/writingModes.png?token=ff1a1aa66debaefb4537cbb77ef42526a01490e6)

This image demonstrates logical operations on individual pixels on a screen.
### Observations and Conclusions
The purpose of this lab was to learn how to program the MSP430 to work with a LCD screen. Required functionality asked me to draw a 10x10 pixel box on the screen. A functionality required that the box be moved by ten pixels in the direction of the button pushed. And the bonus functionality was to draw my name on the screen. I completed every functionality and learned a great deal about SPI on the MSP430. I've overheard some rumors about using the setArea subroutine as a more efficient way of drawing a box. Also, there may be another way to move the box faster, without clearing the screen and redrawing the box. To improve my I would look into those ideas.  

### Documentation
None.