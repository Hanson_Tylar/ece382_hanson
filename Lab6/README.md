# Lab 6 - PWM "Robot Motors"

## By C2C Tylar Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
4. [Hardware schematic](#hardware-schematic)
5. [Debugging](#debugging)
6. [Testing methodology or results](#testing-methodology-or-results)
7. [Answers to Lab Questions](#answers-to-lab-questions)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)
 
### Objectives or Purpose 
This lab is designed to provide you with experience using the pulse-width modulation features of the MSP430. You will need to program the MSP430 to generate pulse-width-modulated wave forms to control the speed / direction of your robot's motors. 
- Required Functionality
	- Move the robot forward, backwards, a small (< 45 degree) turn left/right, and a large (> 45 degree) turn left/right.
- A Functionality
	- Control the robot with a remote! Use at least four different buttons on the remote: one each to indicate motion directions of forward, backward, left, and right

### Preliminary design
For the prelab I worked out what kind of signals and on which pins. The software flowchart below shows my approach for starting this lab.

### Software flow chart or algorithms
![](images/Lab6.png)

### Hardware schematic
![](images/Schematic_bb.jpg)

### Debugging
- When setting up all my pins in initMSP I set up my GPIOs as inputs instead of outputs. After fixing this my move forward and backward functions worked. 
- After setting up the infrared sensor and running the program the port 2 interrupt would constantly triggered. I was able to figure out that the DMM was causing enough of a voltage change on port 2.6 that the interrupt would trigger. After removing the DMM from the circuit the robot worked.

### Testing methodology or results
Testing started with getting both wheels to move in the same direction. After much trial and error I found that I set my GPIO pins to inputs instead of outputs. After fixing this both wheels moved as expected. In order to figure out how long of a delay I needed to create 45 and 90 degree turn I used the tiles of the lab floor as markers. Once I found the appropriate delay for a 45 degree turn I doubled it and it was perfect for a 90 degree turn. The links to videos below show required functionality and A functionality.

[A Functionality](https://youtu.be/p-vLXvnf3nc)

[Required Functionality](https://youtu.be/LJQc1VOGwHo)

### Answers to Lab Questions
- P2.1 and P2.5 will provide the PWM signals to the red lead on the left motor and the black lead on the right motor.
- P2.2 and P2.5 will be used as GPIO and will be connected to the black lead of the left motor and the red lead of the right motor.
- I will use TA1CTL  and TA1CCTL registers to set up timer A1.
- A voltage regulator and motor driver chip will be used as shown in the hardware schematic above. Capacitors will be used where needed to reduce noise on a line. 
- Each motor on my robot will move individually so that turning will be easier. 

### Observations and Conclusions
The objective of this lab was to program the MSP430 to generate pulse-width-modulated wave forms to control the speed / direction of a robot's motors. Timer and port interrupts were also used to add remote control functionality to the robot. One thing that makes controlling the robot difficult is that as the batteries die it affects the speed of the motors and causes the robot to veer off to the left or right when going forward. I learned that while debugging the DMM was a great tool to see if the voltage on the pins of the MSP430 are what I expected them to be.

### Documentation
None.