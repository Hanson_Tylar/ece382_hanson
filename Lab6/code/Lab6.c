/*--------------------------------------------------------------------
Name: Tylar Hanson
Date: 15 Nov 16
Course: ECE 382
File: Lab6.c
Event: Lab 6

Purp: Functions to control the movement of a simple robot.

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430g2553.h>
#include "lab6.h"

void initMSP430() {

	WDTCTL=WDTPW+WDTHOLD; 				// stop WD

	TA1CTL |= TASSEL_2|MC_1|ID_0;       // Set up TimerA1 for PWM
	TA1CCR0 = 1000;                		//
	TA1CCR1 = 1000;						//
	TA1CCR2 = 1000;						//
	TA1CCTL1 |= OUTMOD_7;				//
	TA1CCTL2 |=	OUTMOD_7;				//

	P2DIR &= ~BIT6;						// Set up P2.6 to receive IR
	P2SEL &= ~BIT6;						//
	P2SEL2 &= ~BIT6;					//
	P2IFG &= ~BIT6;						//
	P2IE  |= BIT6;						//
	HIGH_2_LOW;							//

	TA0CCR0 = 16000;					// Set up TimerA0 for IR
	TA0CTL &= ~TAIFG;					//
	TA0CTL |= ID_0|TASSEL_2|MC_1|TAIE;  //
	TA0CTL &= ~MC_1;					//


	P2DIR |= BIT1;                		// TA1CCR1 on P2.1
	P2SEL |= BIT1;                		//
	P2OUT &= ~BIT1;						//

	P2DIR |= BIT5;                		// TA1CCR2 on P2.5
	P2SEL |= BIT5;                		//
	P2OUT &= ~BIT5;						//

	P2DIR |= BIT0|BIT3;					// GPIO on P2.0/P2.3 (Left/right motor enables)
	P2SEL &= ~(BIT0|BIT3);				//
	P2SEL2 &= ~(BIT0|BIT3);				//

	P2DIR |= BIT2|BIT4;					// GPIO on P2.2/P2.4 (Left/right motor directional signals)
	P2SEL &= ~(BIT2|BIT4);				//
	P2SEL2 &= ~(BIT2|BIT4);				//

	_enable_interrupt();
}

void requiredFunctionality(){
	stopRobot();
	__delay_cycles(1000000);
	turnCW45();
	__delay_cycles(250000);
	turnCCW45();
	__delay_cycles(250000);
	turnCW90();
	__delay_cycles(250000);
	turnCCW90();
	__delay_cycles(250000);
	moveForward();
	__delay_cycles(1000000);
	moveBackward();
	__delay_cycles(1000000);
	stopRobot();
}

void stopRobot(){
	P2OUT &= ~(BIT0|BIT3);				// Disable left and right motors
	P2OUT &= ~(BIT2|BIT4);				// Directional lines low
	TA1CCR1 = 1000;
	TA1CCR2 = 1000;
	__delay_cycles(1000000);
}

void moveBackward(){
	stopRobot();						// Call stop to prevent sudden changes in direction
	P2OUT |= BIT0|BIT3;					// Enable left and right motors
	P2OUT |= BIT2|BIT4;					// Directional lines high
	TA1CCR1 = 250;
	TA1CCR2 = 250;
}

void moveForward(){
	stopRobot();						// Call stop to prevent sudden changes in direction
	P2OUT |= BIT0|BIT3;					// Enable left and right motors
	P2OUT &= ~(BIT2|BIT4);				// Directional lines low
	TA1CCR1 = 750;
	TA1CCR2 = 750;
}

void turnCCW(){
	stopRobot();						// Call stop to prevent sudden changes in direction
	P2OUT |= BIT0|BIT3;					// Enable left and right motors
	P2OUT |= BIT2;						// Left motor forward
	P2OUT &= ~BIT4;						// Right motor in reverse
	TA1CCR1 = 650;
	TA1CCR2 = 350;
}

void turnCW(){
	stopRobot();						// Call stop to prevent sudden changes in direction
	P2OUT |= BIT0|BIT3;					// Enable left and right motors
	P2OUT |= BIT4;						// Right motor forward
	P2OUT &= ~BIT2;						// Left motor in reverse
	TA1CCR2 = 650;
	TA1CCR1 = 350;
}
void turnCW45(){
	turnCW();
	__delay_cycles(350000);
	stopRobot();
}

void turnCW90(){
	turnCW();
	__delay_cycles(700000);
	stopRobot();
}

void turnCCW45(){
	turnCCW();
	__delay_cycles(350000);
	stopRobot();
}

void turnCCW90(){
	turnCCW();
	__delay_cycles(700000);
	stopRobot();
}
