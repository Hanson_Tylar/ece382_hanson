/*--------------------------------------------------------------------
Name: Tylar Hanson
Date: 15 Nov 16
Course: ECE 382
File: lab6.h
Event: Lab 6

Purp: Include file for the MSP430.

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#ifndef CODE_LAB6_H_
#define CODE_LAB6_H_

typedef		unsigned char		int8;
typedef		unsigned short		int16;
typedef		unsigned long		int32;
typedef		unsigned long long	int64;

#define		TRUE				1
#define		FALSE				0

#define		IR_PIN			(P2IN & BIT6)
#define		HIGH_2_LOW		P2IES |= BIT6
#define		LOW_2_HIGH		P2IES &= ~BIT6

#define		averageLogic0Pulse	0x0235
#define		averageLogic1Pulse	0x0720
#define		averageStartPulse	0x1151
#define		minLogic0Pulse		averageLogic0Pulse - 100
#define		maxLogic0Pulse		averageLogic0Pulse + 100
#define		minLogic1Pulse		averageLogic1Pulse - 100
#define		maxLogic1Pulse		averageLogic1Pulse + 100
#define		minStartPulse		averageStartPulse - 100
#define		maxStartPulse		averageStartPulse + 100

#define		PWR		0x20DF10EF
#define		OK		0x20DF22DD
#define		LEFT	0x20DFE21D
#define		RIGHT	0x20DF12ED
#define		UP		0x20DFA25D
#define		DOWN	0x20DF629D
#define		YELLOW	0x20DF4AB5
#define		BLUE	0x20DFCA35
#define		RED		0x20DF2AD5
#define		GREEN	0x20DFAA55

// Set up Timers, GPIOs, and PWM signals needed to control the robot
void initMSP430();

// Interrupts used in handling IR signal reception
__interrupt void pinChange (void);
__interrupt void timerOverflow (void);

// Demonstrate Required Functionality
void requiredFunctionality();

// Turn off both motors on the robot
void stopRobot();

// Move both motors forward
void moveForward();

// Move the robot in reverse
void moveBackward();

// Turn the robot clockwise when looking down on it
void turnCW();

// Turn the robot counter clockwise when looking down on it
void turnCCW();

void turnCW45();
void turnCCW45();
void turnCW90();
void turnCCW90();

#endif
