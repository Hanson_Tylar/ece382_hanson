# Lab 7 - A/D Conversion "Robot Sensing"

## By C2C Tylar Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
4. [Hardware schematic](#hardware-schematic)
5. [Debugging](#debugging)
6. [Testing methodology or results](#testing-methodology-or-results)
7. [Answers to Lab Questions](#answers-to-lab-questions)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)
 
### Objectives or Purpose 
This lab is designed to assist you in learning the concepts associated with the input capture features for your MSP430. A single ultrasonic rangefinder is attached to a servo motor that can rotate. You will program your MSP430 to use the rangefinder to determine whether your mobile robot is approaching a wall in front or on one of its sides. The skills you will learn from this lab will come in handy in the future as you start interfacing multiple systems.

- Required Functionality
	- Use the Timer_A subsystem to light LEDs based on the presence of a wall. The presence of a wall on the left side of the robot should light LED1 on your Launchpad board. The presence of a wall next to the right side should light LED2 on your Launchpad board. A wall in front should light both LEDs. Demonstrate that the LEDs do not light until the sensor comes into close proximity with a wall.
- B Functionality
	- You need to fully characterize the sensor for your robot. Create a table and graphical plot with at least three data points that shows the rangefinder pulse lengths for a variety of distances from a maze wall to the front/side of your robot.
- A Functionality
	- Control your servo position with your remote! Use remote buttons other than those you have used for the motors. Note: you still need to be able to get readings from the ultrasonic sensor after changing your servo position with the remote.
- Bonus
	- Create a standalone library for your ultrasonic code and release it on Bitbucket. This should be separate folder from your lab code. It should have a thoughtful interface and README, capable of being reused in the robot maze laboratory. This particular repository will remain private.
	
### Prelab
Part I - Understanding the Ultrasonic Sensor and Servo
------------------------------------------------------

#### Ultrasonic Sensor 
1.  How fast does the signal pulse from the sensors travel? 340 M/s

2.  If the distance away from the sensor is 1 in, how long does it take for the sensor pulse to return to the sensor?  296 us
	1.  1 cm? 116 us

3.  What is the range and accuracy of the sensor? Range: 2 cm - 4 m. Accuracy: 3 0m. 

4.  What is the minimum recommended delay cycle (in ms) for using the sensor?  50 ms
	1.  How does this compare to the "working frequency"? The module outputs 40 Hz signals.

#### Servo
1.  Fill out the following table identifying the pulse lengths needed for each servo position:

| Servo Position | Pulse Length (ms) | Pulse Length (counts) |
|----------------|:-----------------:|:---------------------:|
| Left           |         1         |         1000          |
| Middle         |        1.5        |         1500          |
| Right          |         2         |         2000          |


Part II - Using the Ultrasonic Sensor and Servo
-----------------------------------------------

1. Create psuedocode and/or flowchart showing how you will *setup* and *use* the ultrasonic sensor and servo.

2. Create a schematic showing how you will setup the ultrasonic sensor and servo.

**Below are some things you should think about as you design your interfaces:**

 - **Consider if/how you will configure the input capture subsystem** for your ultrasonic sensor.  What are the registers you will need to use?  Which bits in those registers are important?  What is the initialization sequence you will need?  Should you put a limit on how long to sense?  If so, how long makes sense given the limitations of the sensor (or the requirements of the project)?
	 - I will use port interrupts and the timer to measure the length of the signal recieved by the ultrasonic sensor. The maximum sensing time will be 50 ms.

 - **Consider the hardware interface.**  Which signals will you use?  Which pins correspond to those signals?  How will you send a particular pulse width to the servo?
	 - The servo PWM will be managed with TA0CCR0, TA0CCR1, and OUTMOD_7. The signal will be put on P1.2

 - **Consider the software interface you will create to your sensor.**  Will you block or use interrupts?  Will you stop moving while you sense?
	 - I will use interrupts and the sensor will stop moving while it senses.

 - Will the ultrasonic sensor be ready to sense immediately after the servo changes position?  How do you know? 
	 - No, The servo PWM signal needs to stop before the sensor can be used. 

 - How long should you keep sending PWM pulses?  Keep in mind that you may have to send many PWM pulses before your servo is in the correct position.  Even after that, can you stop sending PWM pulses?
	 - The operating speed is 0.1 s/60 degree, so to move 180 degrees it would take 0.3 seconds. I'll probably send PWM pulses for 0.5 seconds. After the servo is in position the PWM pulses can be stopped.

### Software flow chart or algorithms
![](images/Lab7_Flowgraph.png)

### Hardware schematic
![](images/RobotSchematic_bb.jpg)

### Debugging


### Testing methodology or results
##### Characterizing the sensor
The following data classifies the ultrasonic sensors. By placing the robot at certain distances from a wall and timing how long the echo signal is high I created the table and graph below.

![](images/sensor_table.JPG)
![](images/sensor_graph.JPG)

### Answers to Lab Questions


### Observations and Conclusions


### Documentation
None.