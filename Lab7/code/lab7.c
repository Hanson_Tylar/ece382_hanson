/*--------------------------------------------------------------------
Name: Tylar Hanson
Date: 18 Nov 16
Course: ECE 382
File: lab7.c
Event: Lab 7

Purp: Functions to control robot servo and ultrasonic sensors.

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430g2553.h>
#include "lab7.h"

void initMSP(){

	WDTCTL = WDTPW + WDTHOLD;			// Stop WDT

	TA0CTL &= ~TAIFG;					// Set up TimerA0 but don't turn it on
	TA0CTL |= ID_0|TASSEL_2;		  	//

	P1DIR |= BIT2;						// Set up 1.2 as GPIO for servo PWM
	P1SEL |= BIT2;						//
	P1SEL2 &= BIT2;						//
	P1OUT &= ~BIT2;						//
	P1IE &= ~BIT2;						//

	P2DIR |= BIT7;						// Set up 2.7 as output GPIO for sensor trigger
	P2SEL &= ~BIT7;						//
	P2SEL2 &= ~BIT7;					//
	P2OUT &= ~BIT7;						//

	P1DIR &= ~BIT4;						// Set up 1.4 as input GPIO for sensor echo
	P1SEL &= ~BIT4;						//
	P1SEL2 &= ~BIT4;					//
	P1OUT &= ~BIT4;						//
	P1IFG &= ~BIT4;						//
	P1IES &= ~BIT4;						// Low to high edge interrupt
	P1IE |= BIT4;						//

	P1DIR |= BIT0|BIT6;					// Set up red and green LEDs
	P1SEL &= ~(BIT0|BIT6);				//
	P1SEL2 &= ~(BIT0|BIT6);				//
	P1OUT &= ~(BIT0|BIT6);				//
	P1IE &= ~(BIT0|BIT6);				//

	P2DIR &= ~BIT6;						// Set up P2.6 to receive IR
	P2SEL &= ~BIT6;						//
	P2SEL2 &= ~BIT6;					//
	P2IFG &= ~BIT6;						//
	P2IE  |= BIT6;						//
	IR_HIGH_2_LOW;						//



	__enable_interrupt();
}

void getDistance(){
	TA0CCR0 = 50000;					// Set up period for ultrasonic sensor
	P2OUT |= BIT7;						// Send sensor 10us trigger
	__delay_cycles(6);					//
	P2OUT &= ~BIT7;						//
}

int eyesRight(){
	TA0CCR0 = 20000;					// Set up period for servo PWM
	TA0CCR1 = 550;						// ~1ms for 90 deg right
	TA0CTL |= MC_1;				 		//
	TA0CCTL1 |= OUTMOD_7;				//
	__delay_cycles(500000);
	TA0CCTL1 |= OUTMOD_0;				// After a delay, stop sending PWM to servo
	TA0CTL &= ~MC_1;					//
	return EYES_RIGHT;
}

int eyesFront(){
	TA0CCR0 = 20000;					// Set up period for servo PWM
	TA0CCR1 = 1650;						// ~1.5ms for front
	TA0CTL |= MC_1;  					//
	TA0CCTL1 |= OUTMOD_7;				//
	__delay_cycles(500000);
	TA0CCTL1 |= OUTMOD_0;				// After a delay, stop sending PWM to servo
	TA0CTL &= ~MC_1;
	return EYES_FRONT;
}

int eyesLeft(){
	TA0CCR0 = 20000;					// Set up TimerA0 for servo PWM
	TA0CCR1 = 2700;						// ~2ms for 90 deg left
	TA0CTL |= MC_1;  					//
	TA0CCTL1 |= OUTMOD_7;				//
	__delay_cycles(500000);
	TA0CCTL1 |= OUTMOD_0;				// After a delay, stop sending PWM to servo
	TA0CTL &= ~MC_1;
	return EYES_LEFT;
}
