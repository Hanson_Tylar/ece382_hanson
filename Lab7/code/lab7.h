/*--------------------------------------------------------------------
Name: Tylar Hanson
Date: 18 Nov 16
Course: ECE 382
File: lab7.h
Event: Lab 7

Purp: Lab 7 Header file.

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#ifndef LAB7_H_
#define LAB7_H_

#define 	EYES_LEFT 			0
#define		EYES_FRONT			1
#define		EYES_RIGHT			2

typedef		unsigned char		int8;
typedef		unsigned short		int16;
typedef		unsigned long		int32;
typedef		unsigned long long	int64;

#define		TRUE				1
#define		FALSE				0

#define		ECHO_PIN			(P1IN & BIT4)
#define		ECHO_LOW_2_HIGH		P1IES &= ~BIT4
#define		ECHO_HIGH_2_LOW		P1IES |= BIT4
#define		IR_PIN				(P2IN & BIT6)
#define		IR_HIGH_2_LOW		P2IES |= BIT6
#define		IR_LOW_2_HIGH		P2IES &= ~BIT6

#define		averageLogic0Pulse	0x0235
#define		averageLogic1Pulse	0x0720
#define		averageStartPulse	0x1151
#define		minLogic0Pulse		averageLogic0Pulse - 100
#define		maxLogic0Pulse		averageLogic0Pulse + 100
#define		minLogic1Pulse		averageLogic1Pulse - 100
#define		maxLogic1Pulse		averageLogic1Pulse + 100
#define		minStartPulse		averageStartPulse - 100
#define		maxStartPulse		averageStartPulse + 100

#define		PWR		0x20DF10EF
#define		OK		0x20DF22DD
#define		LEFT	0x20DFE21D
#define		RIGHT	0x20DF12ED
#define		UP		0x20DFA25D
#define		DOWN	0x20DF629D
#define		YELLOW	0x20DF4AB5
#define		BLUE	0x20DFCA35
#define		RED		0x20DF2AD5
#define		GREEN	0x20DFAA55

// Set up the MSP to use the ultrasonic sensor and leds
void initMSP();

// Trigger the sensor by sending a 10 microsecond pulse
void getDistance();

// Rotate the robot eyes to specified location and return integer relating to where the eyes are
int eyesRight();
int eyesLeft();
int eyesFront();

#endif
