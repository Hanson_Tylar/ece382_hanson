/*--------------------------------------------------------------------
Name: Tylar Hanson
Date: 18 Nov 16
Course: ECE 382
File: main.c
Event: Lab 7

Purp: Demonstrate control of ultrasonic sensors on robot from lab6.

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430g2553.h>
#include "lab7.h"

unsigned int distance = 0;		// Distance in inches
int eyes_direction = EYES_FRONT;

int8	newIrPacket = FALSE;
int16	packetData[48];
int8	packetIndex = 0;
int32	irPacket = 0;

void main(void){

	initMSP();
	while(1){
		if (packetIndex > 33){
			packetIndex = 0;
		}
		if(newIrPacket){
			switch(irPacket){
				case UP:
					eyes_direction = eyesFront();
					getDistance();
					break;
				case LEFT:
					eyes_direction = eyesLeft();
					getDistance();
					break;
				case RIGHT:
					eyes_direction = eyesRight();
					getDistance();
					break;
				case OK:
					getDistance();
					break;
			}
		}
		newIrPacket = FALSE;
	}
}

#pragma vector = PORT1_VECTOR
__interrupt void EchoPinChange (void){

	if(ECHO_PIN){
		TA0R = 0;
		TA0CTL |= MC_1;
		ECHO_HIGH_2_LOW;
	}
	else{
		TA0CTL &= ~MC_1;
		distance = (TA0R / 148) - 2;
		ECHO_LOW_2_HIGH;
		if(distance <= 12){
			switch(eyes_direction){
				case EYES_FRONT:
					P1OUT |= BIT0|BIT6;
					break;
				case EYES_LEFT:
					P1OUT |= BIT0;
					P1OUT &= ~BIT6;
					break;
				case EYES_RIGHT:
					P1OUT |= BIT6;
					P1OUT &= ~BIT0;
					break;
			}
		}
		else{
			P1OUT &= ~(BIT0|BIT6);
		}
	}
	P1IFG &= ~BIT4;
}

#pragma vector = PORT2_VECTOR			// This is from the MSP430G2553.h file
__interrupt void IRpinChange (void) {

	int8	pin;
	int16	pulseDuration;						// The timer is 16-bits
	TA0CCR0 = 16000;							// Set up TimerA0 period for IR


	if (IR_PIN)		pin=1;	else pin=0;

	switch (pin) {								// read the current pin level
		case 0:									// !!!!!!!!!NEGATIVE EDGE!!!!!!!!!!
			pulseDuration = TA0R;				//**Note** If you don't specify TA1 or TA0 then TAR defaults to TA0R
			if(TA0R >= minLogic1Pulse && TA0R <= maxLogic1Pulse){
				irPacket <<= 1;
				irPacket |= 0x0001;
			}
			else{
				irPacket <<= 1;
				irPacket &= ~0x0001;
			}
			packetData[packetIndex++] = pulseDuration;
			TA0CTL &= ~MC_1;
			IR_LOW_2_HIGH; 						// Set up pin interrupt on positive edge
			break;

		case 1:									// !!!!!!!!POSITIVE EDGE!!!!!!!!!!!
			TA0R = 0x0000;						// time measurements are based at time 0
			TA0CTL |= MC_1|TAIE;				// Turn on timer A, enable timer A interrupt
			IR_HIGH_2_LOW; 						// Set up pin interrupt on falling edge
			break;
	}
	P2IFG &= ~BIT6;			// Clear the interrupt flag to prevent immediate ISR re-entry
}

#pragma vector = TIMER0_A1_VECTOR			// This is from the MSP430G2553.h file
__interrupt void timerOverflow (void) {
	TA0CTL &= ~(MC_1|TAIE);					// Turn off timer A, Turn off timer A interrupt
	packetIndex = 0;						// Clear packet index
	newIrPacket = TRUE;						// set newPacket flag
	TA0CTL &= ~TAIFG;						// Clear TAIFG
}
