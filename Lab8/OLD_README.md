# Lab 8 - Robot Maze

## By C2C Tylar Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
4. [Hardware schematic](#hardware-schematic)
5. [Debugging](#debugging)
6. [Testing methodology or results](#testing-methodology-or-results)
7. [Answers to Lab Questions](#answers-to-lab-questions)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)
 
### Objectives or Purpose 
Combine the previous laboratory assignments and program a robot to autonomously navigate through a maze.

1. Your robot must always start at the home position.
2. Your robot is considered successful only if it finds one of the three exits and moves partially out of the maze.
3. A large portion of your grade depends on which door you exit.
  1. Door 1 - Required Functionality
  2. Door 2 - B Functionality
  3. Door 3 - A Functionality
    1. **You cannot hit a wall!**
  4. Bonus!  Navigate from the A door back to the entrance using the same algorithm.
    1. **You cannot hit a wall!**
4. Your robot must solve the maze in less than three minutes.
5. Your robot will be stopped if it touches the wall more than twice.
6. Your robot must use the ultrasonic sensor to find its path through the maze.

### Preliminary design
![](images/maze_diagram.png)
	
### Software flow chart or algorithms
![](images/Lab8.png)

### Hardware schematic
If you are wiring things up you will need to create a schematic for your design.

### Debugging
The biggest problem I had with the maze is my robot not turning in the direction I expected it to. To fix this I had to adjust the checking distances in `mazeForward()` and `checkCorner()` to follow more closely to the left wall. After doing this the robot rarely turned in the wrong direction.

### Testing methodology or results
Testing the functionality of the robot was the biggest part of this lab. One of the things I tested was the accuracy of my robot's movement. Using the lines of the lab floor I would correct any drift when the robot moves forward. Also using the lines on the lab floor and continually calling `turnCW90()` or `turnCCW90()` I was able to adjust the delay for more precise 90 degree turns.

### Observations and Conclusions
During this whole assignment, what did you learn?  What did you notice that was noteworthy?  This should be a paragraph starting with the purpose, whether or not you achieved that purpose, what you learned, and how you can use this for future labs.

### Documentation
None.