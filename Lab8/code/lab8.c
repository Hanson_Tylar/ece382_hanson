/*--------------------------------------------------------------------
Name: Tylar Hanson
Date: 2 Dec 16
Course: ECE 382
File: Lab8.c
Event: Lab 8

Purp: Functions to setup and control sensors, motors, and the servo.

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430.h>
#include "lab8.h"

void initMSP(){
	WDTCTL = WDTPW + WDTHOLD;			// Stop WDT

	TA0CTL |= ID_0|TASSEL_2;			// Set up TimerA0 but don't turn it on

	TA1CTL |= TASSEL_2|MC_1|ID_0;       // Set up TimerA1 for  motor PWM
	TA1CCR0 = 1000;                		//
	TA1CCR1 = 1000;						//
	TA1CCR2 = 1000;						//
	TA1CCTL1 |= OUTMOD_7;				//
	TA1CCTL2 |=	OUTMOD_7;				//

	P1DIR |= BIT2;						// Set up 1.2 as GPIO for servo PWM
	P1SEL |= BIT2;						//
	P1SEL2 &= BIT2;						//
	P1OUT &= ~BIT2;						//
	P1IE &= ~BIT2;						//

	P2DIR |= BIT7;						// Set up 2.7 as output GPIO for sensor trigger
	P2SEL &= ~BIT7;						//
	P2SEL2 &= ~BIT7;					//
	P2OUT &= ~BIT7;						//

	P1DIR &= ~BIT4;						// Set up 1.4 as input GPIO for sensor echo
	P1SEL &= ~BIT4;						//
	P1SEL2 &= ~BIT4;					//
	P1OUT &= ~BIT4;						//
	P1IFG &= ~BIT4;						//
	ECHO_LOW_2_HIGH;					// Low to high edge interrupt
	P1IE |= BIT4;						//

	P2DIR |= BIT1;                		// TA1CCR1 on P2.1 (Left Motor Speed)
	P2SEL |= BIT1;                		//
	P2OUT &= ~BIT1;						//

	P2DIR |= BIT5;                		// TA1CCR2 on P2.5 (Right Motor Speed)
	P2SEL |= BIT5;                		//
	P2OUT &= ~BIT5;						//

	P2DIR |= BIT0|BIT3;					// GPIO output on P2.0/P2.3 (Left/right motor enables)
	P2SEL &= ~(BIT0|BIT3);				//
	P2SEL2 &= ~(BIT0|BIT3);				//

	P2DIR |= BIT2|BIT4;					// GPIO output on P2.2/P2.4 (Left/right motor directional signals)
	P2SEL &= ~(BIT2|BIT4);				//
	P2SEL2 &= ~(BIT2|BIT4);				//

	P1DIR |= BIT0|BIT6;					// Set up red and green LEDs
	P1SEL &= ~(BIT0|BIT6);				//
	P1SEL2 &= ~(BIT0|BIT6);				//
	P1OUT &= ~(BIT0|BIT6);				//
	P1IE &= ~(BIT0|BIT6);				//

	_enable_interrupt();
}

void getDistance(){
	TA0CCR0 = 50000;					// Set up period for ultrasonic sensor
	P2OUT |= BIT7;						// Send sensor 10us trigger
	__delay_cycles(6);					//
	P2OUT &= ~BIT7;						//
}

int eyesRight(){
	TA0CCR0 = 20000;					// Set up period for servo PWM
	TA0CCR1 = 550;						// ~1ms for 90 deg right
	TA0CTL |= MC_1;				 		//
	TA0CCTL1 |= OUTMOD_7;				//
	__delay_cycles(500000);
	TA0CCTL1 |= OUTMOD_0;				// After a delay, stop sending PWM to servo
	TA0CTL &= ~MC_1;					//
	return EYES_RIGHT;
}

int eyesFront(){
	TA0CCR0 = 20000;					// Set up period for servo PWM
	TA0CCR1 = 1650;						// ~1.5ms for front
	TA0CTL |= MC_1;  					//
	TA0CCTL1 |= OUTMOD_7;				//
	__delay_cycles(500000);
	TA0CCTL1 |= OUTMOD_0;				// After a delay, stop sending PWM to servo
	TA0CTL &= ~MC_1;
	return EYES_FRONT;
}

int eyesLeft(){
	TA0CCR0 = 20000;					// Set up TimerA0 for servo PWM
	TA0CCR1 = 2700;						// ~2ms for 90 deg left
	TA0CTL |= MC_1;  					//
	TA0CCTL1 |= OUTMOD_7;				//
	__delay_cycles(500000);
	TA0CCTL1 |= OUTMOD_0;				// After a delay, stop sending PWM to servo
	TA0CTL &= ~MC_1;
	return EYES_LEFT;
}

void stopRobot(){
	P2OUT &= ~(BIT0|BIT3);				// Disable left and right motors
	P2OUT &= ~(BIT2|BIT4);				// Directional lines low
	TA1CCR1 = 1000;
	TA1CCR2 = 1000;
	__delay_cycles(500000);
}

void moveBackward(){
	stopRobot();						// Call stop to prevent sudden changes in direction
	P2OUT |= BIT0|BIT3;					// Enable left and right motors
	P2OUT |= BIT2|BIT4;					// Directional lines high
	TA1CCR1 = 250;
	TA1CCR2 = 250;
}

void moveForward(){
	stopRobot();						// Call stop to prevent sudden changes in direction
	P2OUT |= BIT0|BIT3;					// Enable left and right motors
	P2OUT &= ~(BIT2|BIT4);				// Directional lines low
	TA1CCR1 = SPEED_LEFT_FORWARD;
	TA1CCR2 = SPEED_RIGHT_FORWARD;
}

void turnCCW(){
	stopRobot();						// Call stop to prevent sudden changes in direction
	P2OUT |= BIT0|BIT3;					// Enable left and right motors
	P2OUT |= BIT2;						// Left motor forward
	P2OUT &= ~BIT4;						// Right motor in reverse
	TA1CCR1 = 450;
	TA1CCR2 = 550;
}

void turnCW(){
	stopRobot();						// Call stop to prevent sudden changes in direction
	P2OUT |= BIT0|BIT3;					// Enable left and right motors
	P2OUT |= BIT4;						// Right motor forward
	P2OUT &= ~BIT2;						// Left motor in reverse
	TA1CCR2 = 450;
	TA1CCR1 = 550;
}
void turnCW45(){
	turnCW();
	__delay_cycles(350000);
	stopRobot();
}

void turnCW90(){
	turnCW();
	__delay_cycles(260000);
	stopRobot();
}

void turnCCW45(){
	turnCCW();
	__delay_cycles(350000);
	stopRobot();
}

void turnCCW90(){
	turnCCW();
	__delay_cycles(265000);
	stopRobot();
}

void testFunctionality(){
	stopRobot();
	__delay_cycles(1000000);
	turnCW45();
	__delay_cycles(250000);
	turnCCW45();
	__delay_cycles(250000);
	turnCW90();
	__delay_cycles(250000);
	turnCCW90();
	__delay_cycles(250000);
	moveForward();
	__delay_cycles(1000000);
	moveBackward();
	__delay_cycles(1000000);
	stopRobot();
	eyesRight();
	eyesLeft();
	eyesFront();
}
