#ifndef LAB8_H_
#define LAB8_H_

#define 	EYES_LEFT 			0
#define		EYES_FRONT			1
#define		EYES_RIGHT			2

typedef		unsigned char		int8;
typedef		unsigned short		int16;
typedef		unsigned long		int32;
typedef		unsigned long long	int64;

#define		TRUE				1
#define		FALSE				0

#define		ECHO_PIN			(P1IN & BIT4)
#define		ECHO_LOW_2_HIGH		P1IES &= ~BIT4
#define		ECHO_HIGH_2_LOW		P1IES |= BIT4
#define		SPEED_LEFT_FORWARD	740
#define		SPEED_RIGHT_FORWARD	770


// Set up the MSP to use the ultrasonic sensor and leds
void initMSP();

// Trigger the sensor by sending a 10 microsecond pulse
void getDistance();

// Rotate the robot eyes to specified location and return integer relating to where the eyes are
int eyesRight();
int eyesLeft();
int eyesFront();

// Turn off both motors on the robot
void stopRobot();

// Move both motors forward
void moveForward();

// Move the robot in reverse
void moveBackward();

// Turn the robot clockwise when looking down on it
void turnCW();

// Turn the robot counter clockwise when looking down on it
void turnCCW();

void turnCW45();
void turnCCW45();
void turnCW90();
void turnCCW90();

// Test all movement functions of robot motors
void testFunctionality();

// Move robot forward until it is within 8 inches of forward obstacle
void mazeForward();

#endif
