/*--------------------------------------------------------------------
Name: Tylar Hanson
Date: 2 Dec 16
Course: ECE 382
File: main.c
Event: Lab 8

Purp: Navigate a robot through a maze.

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430.h>
#include "lab8.h"

unsigned int distance = 0;
void mazeForward();
void checkCorner();

void main(void) {
	initMSP();
	eyesFront();
	while(1){
//		turnCCW90();
//		__delay_cycles(1000000);
		mazeForward();
		checkCorner();
	}
}

void checkCorner(){
	eyesLeft();
	getDistance();
	__delay_cycles(200000);
	if(distance >= 16){
		turnCCW90();
	}
	else{
		turnCW90();
	}
}

void mazeForward(){
	eyesFront();
	getDistance();
	__delay_cycles(200000);
	if(distance>=14){
		moveForward();
		while(distance>=16){
			__delay_cycles(50000);
			getDistance();
		}
		stopRobot();
	}
}

#pragma vector = PORT1_VECTOR
__interrupt void EchoPinChange (void){
	if(ECHO_PIN){
		TA0R = 0;
		TA0CTL |= MC_1;
		ECHO_HIGH_2_LOW;
	}
	else{
		TA0CTL &= ~MC_1;
		distance = (TA0R / 148) - 2;
		ECHO_LOW_2_HIGH;
	}
	P1IFG &= ~BIT4;
}
