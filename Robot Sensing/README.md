# Robot Sensing Library

## By C2C Tylar Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Hardware ](#hardware)
3. [Documentation](#documentation)

### Objectives or Purpose
This Library explains how to use the given functions to get distances from an ultrasonic sensor and to control the direction of the servo. The function main() in `main.c` serves as an example of how to use the functions in `Robot_Sensing.h` and `Robot_Sensing.c`.

### Hardware

##### Hardware Schematic - Fritzing
![](images/RobotSchematic_bb.jpg)

##### Robot Used in Testing
![](images/Robot.jpg)

### Documentation

More detailed information about the process of building the robot can be found [here](https://bitbucket.org/Hanson_Tylar/ece382_hanson/src) under Lab 6, 7, and 8.

The following data sheets provide device specific information on the MSP430, the HC-SR04 ultrasonic sensor, and SG90 micro servo.

[MSP430](http://ece.ninja/382/datasheets/msp430g2x53_2x13_mixed_sig_mcu.pdf)

[Sensor](http://ece.ninja/382/datasheets/HC-SR04.pdf)

[Servo](http://ece.ninja/382/datasheets/SG92R_servo_datasheet.pdf)

