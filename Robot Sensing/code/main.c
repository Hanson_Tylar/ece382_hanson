/*--------------------------------------------------------------------
Name: Tylar Hanson
Date: 2 Dec 16
Course: ECE 382
File: main.c
Event: Robot Sensing Library

Purp: Demonstrate control of ultrasonic sensors and servo on a robot.

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430g2553.h>
#include "Robot_Sensing.h"

unsigned int distance = 0;							// Distance in inches
int eyes_direction = EYES_FRONT;					// Used to keep track of where the eyes are

int8	newIrPacket = FALSE;
int16	packetData[48];
int8	packetIndex = 0;
int32	irPacket = 0;

void main(void){

	initMSP();
	while(1){
		if (packetIndex > 33){
			packetIndex = 0;
		}
		if(newIrPacket){							// On a button press do the appropriate action
			switch(irPacket){
				case UP:
					eyes_direction = eyesFront();
					break;
				case LEFT:
					eyes_direction = eyesLeft();
					break;
				case RIGHT:
					eyes_direction = eyesRight();
					break;
				case OK:
					getDistance();
					break;
			}
		}
		newIrPacket = FALSE;
	}
}

#pragma vector = PORT1_VECTOR
__interrupt void EchoPinChange (void){
// Finds length of time the echo pin is high, calculates distance to obstacle, and turns on LEDs accordingly

	if(ECHO_PIN){								// Rising Edge
		TA0R = 0;								// Start Timer
		TA0CTL |= MC_1;
		ECHO_HIGH_2_LOW;						// Change interrupt to falling edge
	}
	else{
		TA0CTL &= ~MC_1;						// Stop timing
		distance = (TA0R / 148) - 2;			// Distance in inches
		ECHO_LOW_2_HIGH;						// Enable rising edge trigger to capture the next echo
		if(distance <= 12){						// Turn on LEDs if the robot is within 1 foot of an obstacle
			switch(eyes_direction){
				case EYES_FRONT:
					P1OUT |= BIT0|BIT6;			// Turn on both LEDs for obstacle in front of the robot
					break;
				case EYES_LEFT:
					P1OUT |= BIT0;				// Turn on only LED 1 for obstacle on the left side of the robot
					P1OUT &= ~BIT6;
					break;
				case EYES_RIGHT:
					P1OUT |= BIT6;				// Turn on only LED 2 for obstacle on the right side of the robot
					P1OUT &= ~BIT0;
					break;
			}
		}
		else{
			P1OUT &= ~(BIT0|BIT6);				// If more than a foot from any obstacle away turn of both lights
		}
	}
	P1IFG &= ~BIT4;
}

#pragma vector = PORT2_VECTOR
__interrupt void IRpinChange (void) {
// Decode an infrared signal

	int8	pin;
	int16	pulseDuration;						// The timer is 16-bits
	TA0CCR0 = 16000;							// Set up TimerA0 period for IR

	if (IR_PIN)	pin=1;	else pin=0;

	switch (pin) {								// read the current pin level
		case 0:									// NEGATIVE EDGE
			pulseDuration = TA0R;
			if(TA0R >= minLogic1Pulse && TA0R <= maxLogic1Pulse){
				irPacket <<= 1;
				irPacket |= 0x0001;
			}
			else{
				irPacket <<= 1;
				irPacket &= ~0x0001;
			}
			packetData[packetIndex++] = pulseDuration;
			TA0CTL &= ~MC_1;
			IR_LOW_2_HIGH; 						// Set up pin interrupt on positive edge
			break;

		case 1:									// POSITIVE EDGE
			TA0R = 0x0000;						// time measurements are based at time 0
			TA0CTL |= MC_1|TAIE;				// Turn on timer A, enable timer A interrupt
			IR_HIGH_2_LOW; 						// Set up pin interrupt on falling edge
			break;
	}
	P2IFG &= ~BIT6;								// Clear the interrupt flag to prevent immediate ISR re-entry
}

#pragma vector = TIMER0_A1_VECTOR
__interrupt void timerOverflow (void) {
// Handle Timer A0 rollovers
	TA0CTL &= ~(MC_1|TAIE);						// Turn off timer A, Turn off timer A interrupt
	packetIndex = 0;							// Clear packet index
	newIrPacket = TRUE;							// Tells main there is a new IR Packet
	TA0CTL &= ~TAIFG;
}
